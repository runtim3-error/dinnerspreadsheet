function onOpen() {  
    var ui = SpreadsheetApp.getUi();
    var menu = ui.createMenu('Работа с заказом');
    var dinnerTable = ui.createMenu('Таблица обеды');
    dinnerTable.addItem('Показать сумму заказа на человека', ShowOrder.name);
    dinnerTable.addItem('Показать предварительный заказ', ShowPreview.name);  
    dinnerTable.addItem('Отправка заказа на почту...(ввести почту вручную)', SendMessageTo.name);
    dinnerTable.addItem('Очистка заказов', Clear.name);
    menu.addSubMenu(dinnerTable)
    
    var currentTable = ui.createMenu('Текущая таблица');
    currentTable.addItem('Показать сумму заказа на человека', ShowOrderByActive.name);
    currentTable.addItem('Показать предварительный заказ', ShowPreviewByActive.name);
    currentTable.addItem('Отправить заказ на почту...(ввести почту вручную)', SendMessageToByActive.name);
    currentTable.addItem('Очистка заказов', ClearByActive.name);
    menu.addSubMenu(currentTable);
    menu.addToUi();
}

function onEdit(e: { range: { getRow: () => number; getColumn: () => number; getSheet: () => GoogleAppsScript.Spreadsheet.Sheet; }; value: string; }){
    var row = e.range.getRow();
    var column = e.range.getColumn();
    
    if (column <=3){
        return;  
    }
    try { 
        var sheet = e.range.getSheet();
        var firstCell = sheet.getRange(row, 1);
        try{
            parseInt(e.value);
        }
        catch (ex)
        {
            return;
        }
      
        var menuItemValue = firstCell.getValue().toString();
        if (menuItemValue.toLowerCase().indexOf("салат") != 0){
            return; 
        }
        
        var values = sheet.getDataRange().getValues();
        var containerRow = -1;
        
        for (var i = 0; i<values.length;i++){
            if (values[i][0].toString().indexOf("Контейнер 200г") != -1){
                containerRow = i + 1;
                break;
            }
        }
        
        if (containerRow <= 0){
            return;
        }
        
        var userContainerRange = sheet.getRange(containerRow, column);
        
        userContainerRange.setValue(1);
      
    }
    catch (ex){
        Logger.log("ERROR:" + ex.message);
        return;
    }
}
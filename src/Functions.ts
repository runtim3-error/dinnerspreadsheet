const backOfficeSheetName = "Обеды"
const frontOfficeSheetName = "Обеды 2"
const daysInWeek = ['вс.','пн.', 'вт.', 'ср.', 'чт.', 'пт.', 'сб.']
const subject = "Заказ обедов в офис ООО \"Программный центр\"";
const additional = "Примечание: подписать блюда (в т.ч. салаты)";
const cutlery = false;
const pbprogGoogleMail = "program.center.kirov@gmail.com";
const cateringEstablishmentMail = "780466@mail.ru";



function ShowOrder(){
    showOrder(new Order(backOfficeSheetName));
}

function ShowOrderByActive(){
    showOrder(new ActiveSheetOrder());
}

function SendMessageTo(){
    sendMessageTo(new Order(backOfficeSheetName));
}

function SendMessageToByActive(){
    sendMessageTo(new ActiveSheetOrder());
}

function ShowPreview(){
    preview(new Order(backOfficeSheetName));
}

function ShowPreviewByActive(){
    var order = new ActiveSheetOrder();
    order.deliveryInfo.additional = "";
    preview(order);
}

function Clear(){
    clear(backOfficeSheetName);
}

function ClearByActive(){
    var activeSheetName = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet().getName();
    clear(activeSheetName);
}

function showOrder(order:Order){
    Browser.msgBox("Заказ:\\n"+order.getPreviewText(), Browser.Buttons.OK);
}

function sendMessageTo(order:Order){
    if (!showValid()){
        return;
    }
    var mail = Browser.inputBox("Отправить письмо с заказом", "Введите почту на которую будет выполнена отправка заказа:", Browser.Buttons.OK_CANCEL);
    if (mail == "cancel"){
        return;
    }
    sendOrderMessage(false, mail, order);
}

function sendOrderMessage(showMes:boolean, mail:string, order:Order) {
    if (!showValid()){
        return;
    }
    if (showMes === undefined){
        showMes = true;
    }
        
    Logger.clear();
    if (!showValid()){
        return;
    }
    var orderText = order.getOrderText();
    if (!orderText){
        return; 
    }
    
    var res = "yes";
    if (showMes){
        res = Browser.msgBox("Отправить сообщение?\\n"+replaceAll(orderText,'\n','\\n'), Browser.Buttons.YES_NO);
    }  
    if (res != "yes"){    
        return;        
    }
    var sender = new ConcreteMailSender(mail);
    var sendResult = sender.send(orderText, subject);
    if (showMes){
        if (sendResult){
            Browser.msgBox("Заказ отправлен", Browser.Buttons.OK);
        }
        else{
            Browser.msgBox("Не удалось отправить заказ", Browser.Buttons.OK);
        }
    } 
}

function clear(sheetName:string){
    if (sheetName != backOfficeSheetName && sheetName != frontOfficeSheetName){
        return;
    }
    if (!showValid()){
        return;
    }
    var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName);
    if (sheet){
        var data = sheet.getDataRange().getValues();
        var row = 3;
        var col = 4;
        var range = sheet.getRange(row,col,data.length-row+1,data[0].length-col+1);
        range.clearContent();
    }
    
    var paymentSheetName = getPaymentSheetName(sheetName);
    if (!paymentSheetName){
        return;
    }
    var paymentSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(paymentSheetName);
    if (!paymentSheet){
        return;
    }
    col = 4;
    row = 2;
    range = paymentSheet.getRange(row, col, 60);
    range.clearContent();
}

function preview(order:Order){
    Browser.msgBox(replaceAll(order.getOrderText(), "\n","\\n"));
}

function showValid() : boolean{
    var validator = new UserValidator();
    if (!validator.isUserValid()){
        Browser.msgBox("К сожалению у вас нет доступа", Browser.Buttons.OK);
        return false; 
    }
    return true;
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function SendFirstOffice(){
    if (!showValid()){
        return;
    }
    var order = new Order(backOfficeSheetName);
    if (!order){
        return;
    }
    SendOrderAndUpdateTrigger(SendFirstOffice.name,order)
}

function SendSecondOffice(){
    if (!showValid()){
        return;
    }
    var order = new Order(frontOfficeSheetName);
    if (!order){
        return;
    }
    SendOrderAndUpdateTrigger(SendSecondOffice.name,order)
}

function SendOrderAndUpdateTrigger(methodName : string, order:Order){
    if (!showValid()){
        return;
    }
    TriggerUpdater.updateTrigger(methodName);
    try{
        var dayNumber = new Date().getDay();
        if (dayNumber != 0 && dayNumber != 6){
            sendOrderMessage(false, cateringEstablishmentMail, order);
        }
    }
    catch (e){
        Logger.log("Отправка ошибка: " + e)
        return;
    }
}

function IsHoliday() : boolean {
    try{
        var todayDate = new Date();
        var holidaysCalendar = CalendarApp.getCalendarsByName('Праздники РФ')[0];
        return holidaysCalendar && holidaysCalendar.getEventsForDay(todayDate).length > 0;
    }catch (e){
        Logger.log(e)
        return false;
    }
}

function getPaymentSheetName(sheetName:string){
    switch (sheetName){
        case "Обеды":
            return "Оплата";
        case "Обеды 2":
            return "Оплата 2";
        default:
            return undefined;
    }
}


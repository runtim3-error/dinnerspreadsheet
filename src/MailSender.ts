class MailSender{
    validator: UserValidator;
    constructor(){
        this.validator = new UserValidator();
    }

    send(mail:string, content:string, subject:string) : boolean {
        if (!content){
            return false;
        }
        if (!this.validator.isUserValid()){
            return false;
        }
        //var subject = "Заказ обедов в офис ООО \"Программный центр\""; 
        var mails = mail.indexOf('&&')>0
            ? mail.split("&&")
            : [mail];
        for (var i=0; i<mails.length;i++){
            try {
                MailApp.sendEmail(mails[i], subject, content);
            } catch (e) {
                Logger.log("Exception occured when send mail to " + mails[i] + ". " + e);
            }
        }
    }
}

class ConcreteMailSender extends MailSender {
    mail: string;
    constructor(mail : string){
        super();
        this.mail = mail;
    }

    send(content:string, subject:string) : boolean {
        return super.send(this.mail, content, subject);
    }
}
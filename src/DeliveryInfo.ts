class DeliveryInfo {
    address: string;
    contact: string;
    additional: string;
    
    constructor(address: string, contact: string, additional: string){
        this.address = address;
        this.contact = contact;
        this.additional = additional;
    }

    getFooter() : string {
        var endMessages = [
            "Заказ обедов в офис ООО \"Программный центр\"", 
            'Адрес доставки: ' + this.address,
            this.contact,
            this.additional
        ]
        return endMessages.join('\n');
    }
}

class SheetDeliveryInfo extends DeliveryInfo {

    constructor(sheetName:string){
        super(SheetDeliveryInfo.getDeliveryAddress(sheetName), 
            SheetDeliveryInfo.getDeliveryContact(sheetName),
            SheetDeliveryInfo.getDeliveryAdditional(sheetName))
    }

    private static getDeliveryAddress(sheetName:string) : string {
        switch (sheetName){
            case "Обеды":
                return "Московская 110/1, 1 этаж";
            case "Обеды 2":
                return "Московская 107, 1 этаж";
            default:
                return undefined;
        }
    }

    private static getDeliveryContact(sheetName:string) : string {
        switch (sheetName){
            case "Обеды":
                return "Контактные данные: katya@pbprog.ru 8-912-713-8343";
            case "Обеды 2":
                return "Контактные данные: tanyusha-43@pbprog.ru 8-953-943-3902";
            default:
                return undefined;
        }
    }

    private static getDeliveryAdditional(sheetName:string) : string {
        return additional;
    }
}
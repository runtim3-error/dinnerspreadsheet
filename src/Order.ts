class Order {
    sheetName: string;
    deliveryInfo: DeliveryInfo;

    private _orderText:string;
    private _orderMiniText: string;
    private _orderPreviewText: string;

    constructor(sheetName:string)
    constructor(sheetName:string, deliveryInfo?:DeliveryInfo) {
        this.sheetName = sheetName;
        this.deliveryInfo = !deliveryInfo ? new SheetDeliveryInfo(sheetName) : deliveryInfo;
    }

    getOrderText() : string {
        this.updateOrderProperties();
        return this._orderText;
    }

    getOrderTextMini() : string {
        this.updateOrderProperties();
        return this._orderMiniText;
    }

    getPreviewText() : string {
        this.updateOrderProperties();
        return this._orderPreviewText;
    }

    private updateOrderProperties(){
        this._orderText="";
        this._orderMiniText="";
        this._orderPreviewText="";

        var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(this.sheetName);
        var data = sheet.getDataRange().getValues();

        Logger.log(sheet.getName());

        Logger.log(data.length);
        Logger.log(data[0].length);

        var sumAll = 0;
        var columnIndex = 3;
        var personIndexInOrder = 1;
        while (columnIndex != data[0].length) {
            try{
                var personOrder = "";
                var personSum = 0;   
                for (var i=4; i < data.length; i++) {
                    if (data[i][columnIndex]!="") {
                        if (personOrder != ""){
                            personOrder += "\n   ";
                        }
                        
                        var value = data[i][columnIndex] as number;
                        var price = parseFloat(data[i][1].toString());
                        
                        personSum += value * price;       
                        personOrder += "    " + data[i][0] + " - "+ data[i][columnIndex] + " шт.";
                    }
                }
                var additionalCellValue = data[2][columnIndex];
                if (personOrder != "" || additionalCellValue != "") {
                    personSum = Math.round(personSum)
                    this._orderText += personIndexInOrder + ") " + data[0][columnIndex] + " :\n   " + personOrder + (personOrder!=""?", ":"") + additionalCellValue;
                    //order+="\nИтого "+ data[1][man] + " руб.";
                    this._orderText += "\n\n"
                    this._orderPreviewText += data[0][columnIndex] + " - " + personSum + "\\n";
                    this._orderMiniText += data[0][columnIndex] + " - " + personSum + "руб.";      
                    sumAll+=personSum;
                    personIndexInOrder++;
                }   
                Logger.log(this._orderText);
            }
            finally{
                columnIndex++;
            }
            
        }
        if (sumAll == 0){
            this._orderText = "";
            return;
        }
        if (cutlery) {
            this._orderText += "\n" + "Количество приборов = " + (personIndexInOrder-1) + ".\n";
        }
           
        this._orderText +='Итого = ' + sumAll+ ' руб.\n';// (с учетом скидки в '+ activeSale*100 + '%)\n';
        this._orderText += this.deliveryInfo.getFooter();
    }
}

class ActiveSheetOrder extends Order{
    constructor() {
        var sheetName = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet().getName();
        super(sheetName);
    }
}
class UserValidator{

    mails = [
        "4lex.kislitsyn@gmail.com",
        "denislukoyanov@gmail.com", 
        "katenaumk@gmail.com", 
        "alyonka.love12@gmail.com", 
        "program.center.kirov@gmail.com"
    ];

    constructor()
    { }

    isUserValid() : boolean {
        try {
            var mail = Session.getActiveUser().getEmail();
            var index = this.mails.indexOf(mail);
            return (index > -1 || mail == "");
        } catch (e) {
            Logger.log("Exception occured in user validation: " + e)
            return false;
        }
    }
}
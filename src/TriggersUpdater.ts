class TriggerUpdater{
    
    public static updateTrigger(hanleFunction:string) {
        var triggers = ScriptApp.getProjectTriggers();
        for ( var i in triggers ) {
            if (triggers[i].getHandlerFunction() == hanleFunction){
                ScriptApp.deleteTrigger(triggers[i]);
                break;
            }
        }
        
        var nextTriggerDate = new Date();
        // if friday skip two days
        var offset = nextTriggerDate.getDay() != 5 ? 1 : 3;
        nextTriggerDate.setDate(nextTriggerDate.getDate() + offset);
        nextTriggerDate.setHours(10,0,0);
        var triggerBuilder = ScriptApp.newTrigger(hanleFunction);        
        var trigger = triggerBuilder.timeBased().at(nextTriggerDate).create();
    }
}